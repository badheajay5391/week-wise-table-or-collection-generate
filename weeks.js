function getWeeksInMonth(month = null, year = null){
    /* if month and year not present then return current month weeks*/
    if (month === null || year === null) {
        month = new Date().getMonth() + 1;
        year = new Date().getFullYear();
    }
    var weeks=[],
        firstDate = new Date(year, month, 1),
        lastDate = new Date(year, month+1, 0), 
        numDays = lastDate.getDate();
    console.log(firstDate.getDay(), numDays, '...........')
    var start=1;
    var end=7-firstDate.getDay();
    while(start<=numDays){
        /** prepend 0 when start & end day less than 10*/
        inStart = (start < 10)? `0${start}`: start;
        inEnd = (end < 10)? `0${end}`: end;
        weeks.push({start:inStart,end:inEnd});
        start = end + 1;
        end = end + 7;
        if(end>numDays)
            end=numDays;    
    }        
     return weeks;
 }   

 function getWeekCollectionInMonth(month, year) {
    let weeks = getWeeksInMonth(month, year);
    let collections = [];

    if (weeks.length <= 0) {
        throw new Error('Something went wrong in generating collection');
    }
    /** prepend 0 when month less than 10  */
    let inMonth = (month < 10) ? `0${month + 1}`: month + 1;

    for (week of weeks) {
        let collection = `${week.start}_${week.end}_${inMonth}_${year}_transaction`;
        collections.push(collection);
    }
    return collections;
 }

 function getCurrentWeekCollection(date) {
    if (!date) {
        date = new Date();
    }
    day = date.getDate();
    month = date.getMonth();
    year = date.getFullYear(); 
    let weeks = getWeeksInMonth(month, year);
    console.log(weeks, month, '...........')
    let inMonth = (month < 10) ? `0${month + 1 }`: month + 1;
    if (weeks) {
        for (let week of weeks) {
            if (day >= week.start && day <= week.end) {
                let collection = `${week.start}_${week.end}_${inMonth}_${year}_transaction`;
                return collection;
            }
        }
    }
 }
console.log(getWeeksInMonth(05,2019), 'weeks');
console.log(getWeekCollectionInMonth(05,2019), 'collections');
console.log(getCurrentWeekCollection(new Date()), 'current week collection');